"""API Resources are documented in postman"""

from flask_restful import Resource
from models.UserModel import UserModel
from models.GroupModel import GroupModel
from utils import generate_pasword


class Install(Resource):
    def get(self):
        if UserModel.find_by_username("admin"):
            return {"message": "This page doesn't exist"}, 404
        else:
            admin_group = GroupModel(name="admin")
            admin_group.save_to_db()

            password = generate_pasword(15)
            user = UserModel(
                username="admin",
                password=UserModel.generate_hash(password),
                group=admin_group,
            )
            user.save_to_db()

            return {
                "message": "User {} was created and added to group admin. Please login to continue.".format(
                    "admin"
                ),
                "username": "admin",
                "password": password,
            }
