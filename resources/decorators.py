"""Contains decorators aimed to help resource development"""

import functools
from models.UserModel import UserModel
from flask_jwt_extended import get_jwt_identity
from typing import Callable


def admin_required(func: Callable):
    """
    Asserts that the connected user is an admin.
    Returns an error if he is not

    Args
        :param func the function to decorate
    """

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        connected_username = get_jwt_identity()
        connected_user = UserModel.find_by_username(connected_username)

        if connected_user.is_admin():
            return func(*args, **kwargs)
        else:
            return {"message": "Only admin can do that"}

    return wrapper
