"""API Resources are documented in postman"""

from flask_restful import Resource, reqparse
from models.UserModel import UserModel
from models.BroModel import BroModel
from flask_jwt_extended import get_jwt_identity, jwt_required
import datetime


class BroMessages(Resource):
    @jwt_required
    def get(self):
        connected_username = get_jwt_identity()
        user = UserModel.find_by_username(connected_username)
        return {
            "received_messages": [
                {
                    "message": m.message,
                    "timestamp": m.timestamp,
                    "from": m.from_user.username,
                }
                for m in user.received_messages
            ],
            "sended_messages": [
                {
                    "message": m.message,
                    "timestamp": m.timestamp,
                    "to": m.to_user.username,
                }
                for m in user.sended_messages
            ],
        }

    @jwt_required
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument(
            "receiver", help="This field cannot be blank", required=True
        )
        parser.add_argument("message", help="This field cannot be blank", required=True)

        data = parser.parse_args()

        sender_username = get_jwt_identity()
        sender = UserModel.find_by_username(sender_username)

        receiver = UserModel.find_by_username(data["receiver"])
        if not receiver:
            return {"message": "User {} doesn't exist".format(data["receiver"])}

        msg = BroModel(
            message=data["message"],
            timestamp=int(datetime.datetime.now().timestamp()),
            from_user=sender,
            to_user=receiver,
        )

        msg.save_to_db()

        return {
            "message": "message were successfully added"
        }
