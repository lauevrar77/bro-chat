"""API Resources are documented in postman"""

from flask_restful import Resource, reqparse
from models.UserModel import UserModel
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    jwt_refresh_token_required,
    get_jwt_identity,
    get_raw_jwt,
    jwt_required,
)
from models.RevokedTokenModel import RevokedTokenModel
from resources.decorators import admin_required


parser = reqparse.RequestParser()
parser.add_argument("username", help="This field cannot be blank", required=True)
parser.add_argument("password", help="This field cannot be blank", required=True)


class Users(Resource):
    @jwt_required
    def get(self):
        return UserModel.return_all()

    @jwt_required
    @admin_required
    def post(self):
        data = parser.parse_args()

        if UserModel.find_by_username(data["username"]):
            return {"message": "User {} already exists".format(data["username"])}

        new_user = UserModel(
            username=data["username"],
            password=UserModel.generate_hash(data["password"]),
        )

        try:
            new_user.save_to_db()
            access_token = create_access_token(identity=data["username"])
            refresh_token = create_refresh_token(identity=data["username"])
            return {
                "message": "User {} was created".format(data["username"]),
                "access_token": access_token,
                "refresh_token": refresh_token,
            }
        except Exception:
            return {"message": "Something went wrong"}, 500


class PasswordChange(Resource):
    @jwt_required
    def post(self, user_name):
        if not UserModel.find_by_username(user_name):
            return {"message": "User {} doesn't exist".format(user_name)}

        connected_username = get_jwt_identity()
        connected_user = UserModel.find_by_username(connected_username)
        is_admin = connected_user.is_admin()

        parser = reqparse.RequestParser()

        if not is_admin:
            parser.add_argument(
                "old_password", help="This field cannot be blank", required=True
            )
        parser.add_argument(
            "new_password", help="This field cannot be blank", required=True
        )

        data = parser.parse_args()
        user = UserModel.find_by_username(user_name)
        if not is_admin and not UserModel.verify_hash(
            data["old_password"], user.password
        ):
            return {"message": "Bad password for user {}".format(user_name)}

        user.change_password(data["new_password"])

        return {"message": "Password changed successfully"}


class SpecificUser(Resource):
    @jwt_required
    def get(self, user_name):

        if not UserModel.find_by_username(user_name):
            return {"message": "User {} doesn't exist".format(user_name)}

        user = UserModel.find_by_username(user_name)

        return {"username": user.username}

    @jwt_required
    @admin_required
    def delete(self, user_name):
        if user_name == "admin":
            return {"message": "Admin user cannot be removed"}

        if not UserModel.find_by_username(user_name):
            return {"message": "User {} doesn't exist".format(user_name)}

        user = UserModel.find_by_username(user_name)
        UserModel.delete(user)

        return {"message": "User {} removed".format(user_name)}


class UserLogin(Resource):
    def post(self):
        data = parser.parse_args()
        current_user = UserModel.find_by_username(data["username"])

        if not current_user:
            return {"message": "User {} doesn't exist".format(data["username"])}

        if UserModel.verify_hash(data["password"], current_user.password):
            access_token = create_access_token(identity=data["username"])
            refresh_token = create_refresh_token(identity=data["username"])
            return {
                "message": "Logged in as {}".format(current_user.username),
                "access_token": access_token,
                "refresh_token": refresh_token,
            }
        else:
            return {"message": "Wrong credentials"}


class TokenRefresh(Resource):
    @jwt_refresh_token_required
    def post(self):
        current_user = get_jwt_identity()
        access_token = create_access_token(identity=current_user)
        return {"access_token": access_token}


class UserLogoutAccess(Resource):
    @jwt_required
    def post(self):
        jti = get_raw_jwt()["jti"]
        try:
            revoked_token = RevokedTokenModel(jti=jti)
            revoked_token.add()
            return {"message": "Access token has been revoked"}
        except Exception:
            return {"message": "Something went wrong"}, 500


class UserLogoutRefresh(Resource):
    @jwt_refresh_token_required
    def post(self):
        jti = get_raw_jwt()["jti"]
        try:
            revoked_token = RevokedTokenModel(jti=jti)
            revoked_token.add()
            return {"message": "Refresh token has been revoked"}
        except Exception:
            return {"message": "Something went wrong"}, 500
