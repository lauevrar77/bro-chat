from app import api, logger
from resources.InstallResource import Install
from resources.UserResource import (
    Users,
    SpecificUser,
    UserLogin,
    UserLogoutAccess,
    UserLogoutRefresh,
    TokenRefresh,
    PasswordChange,
)
from resources.BroMessagesResource import BroMessages


def register_api_points():
    logger.info("Registring api points  ")
    api.add_resource(Install, "/install")

    api.add_resource(UserLogin, "/login")
    api.add_resource(UserLogoutAccess, "/logout/access")
    api.add_resource(UserLogoutRefresh, "/logout/refresh")
    api.add_resource(TokenRefresh, "/refresh")
    api.add_resource(Users, "/users")
    api.add_resource(SpecificUser, "/users/<string:user_name>")
    api.add_resource(PasswordChange, "/users/<string:user_name>/password_change")

    api.add_resource(BroMessages, "/bros")
