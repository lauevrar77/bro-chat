# Use an official Python runtime as a parent image
FROM python:3.6-slim

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
ADD . /app

# Install any needed packages specified in requirements.txt
RUN pip install pipenv
RUN pipenv install

# Make port 8000 available to the world outside this container
EXPOSE 8000

# Define environment variable
ENV DEV "false"

# Run app.py when the container launches
CMD ["pipenv", "run", "gunicorn", "-b","0.0.0.0:8000", "wsgi"]