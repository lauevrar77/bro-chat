Bro's messages documentation
============================

Introduction
^^^^^^^^^^^^
Bro's message is an application aimed to allow users to send messages to each other with Bros in it.
Just because it's so cool.

Multiple kinds of Bros exist, for instance happy or angry ones.
The only restriction is that each message must contain a Bro !

Table of contents
^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   technical

Indices and tables
^^^^^^^^^^^^^^^^^^

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
