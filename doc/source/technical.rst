Technical aspects
=================

Bro's message is a Python 3.6 application aimed to run in a Dockerized environment and as a restfull web service.

System requirements are :

* A MySQL database

Software requirements are described in *Pipfile* and *Pipfile.lock*. These requirements can be install by running :

::

    pipenv install

This command will create a new local virtualenv before installing dependencies into this one.

Configuration
^^^^^^^^^^^^^

In order to configure connection between Bro'messages several configuration files are mandatory depending on the running environment.

If you are in a development environment, configuration informations are loaded from *configuration.cfg* file.

This file must have the following content (that have to be adapted to your needs) :

::

    SQLALCHEMY_DATABASE_URI="mysql+pymysql://root:passwd@127.0.0.1:3306/metahub"
    SQLALCHEMY_TRACK_MODIFICATIONS=False
    SECRET_KEY="some-secret-string"
    JWT_SECRET_KEY="jwt-secret-string"
    JWT_BLACKLIST_ENABLED=True

In a production environment, the used configuration file is *configuration_prod.cfg*. This file must have the following content (adapted to your needs) :

::

    SQLALCHEMY_DATABASE_URI="mysql+pymysql://root:passwd@db:3306/metahub"
    SQLALCHEMY_TRACK_MODIFICATIONS=False
    SECRET_KEY="b7sZZ4lxPOjAsxY"
    JWT_SECRET_KEY="EPRxsLwJZgOb65i"
    JWT_BLACKLIST_ENABLED=True

The switch between *development* and *production* environments is made through the **DEV** environment variable. This can take two values **True** or **False**. Note that if this environment variable is not found default environment is *developement*.

Docker deployment
^^^^^^^^^^^^^^^^^

Though a local deployment is obvisously possible the main deployment method of this system is through Docker and as a micro service application.

In this purpose, a *Dockerfile* and a *docker-compose.yml* file are given. The *Dockerfile* allows to compile the Bro'messages application as a container where the *docker-compose.yml* allows a complete deployment of the application.

However, for the *docker-compose* to work properly, a *.env* file is needed at the root of this project. This file must have at least the following content :

::

    MYSQL_ROOT_PASSWORD=passwd
    MQTT_URL=localhost

Installation
^^^^^^^^^^^^

When Bro'smessage is up and running for the first time, an installation request must be sent to it in order to create the database schema as well as admin user.

This request can be made this way :

::

    curl -X GET \
    http://localhost:8000/install \
    -H 'cache-control: no-cache'

and will have the following output :

::

    {"message": "User admin was created. Please login to continue.", "username": "admin", "password": "?k>dIPpQej:e5~J"}

**It's important to take note of this admin password as there is no way to get it a second time**.

After this first request, the installation endpoint will not be accessible anymore.

API Documentation
^^^^^^^^^^^^^^^^^

API documentation is made in the postman project. Just ask access to it.

Endpoints security
^^^^^^^^^^^^^^^^^^

The restfull API of the Bro's message uses JWT authentication.
To log into the API, you have to use the following request :

::

    curl -X POST \
    http://localhost:8000/login \
    -H 'Content-Type: application/json' \
    -H 'cache-control: no-cache' \
    -d '{
        "username": "admin",
        "password": "?k>dIPpQej:e5~J"
    }'

This request gives the following result :

::

    {"message": "Logged in as admin", "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MzYwNjA4MjEsIm5iZiI6MTUzNjA2MDgyMSwianRpIjoiNDY4MWE0MTUtYTE2Yy00OTRhLTg3YzktMDU5ZGZkY2JhNjhlIiwiZXhwIjoxNTM2MDYxNzIxLCJpZGVudGl0eSI6ImFkbWluIiwiZnJlc2giOmZhbHNlLCJ0eXBlIjoiYWNjZXNzIn0.hdHzjWeqPzZ8TskytSlhjd8YiVr8z4MpKkVAGgizJ0I", "refresh_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MzYwNjA4MjEsIm5iZiI6MTUzNjA2MDgyMSwianRpIjoiM2RjZDM2OTctMGRjMC00OWZjLTk1ODctZmFlZDEzMDFhYzA1IiwiZXhwIjoxNTM4NjUyODIxLCJpZGVudGl0eSI6ImFkbWluIiwidHlwZSI6InJlZnJlc2gifQ.zZfKQPX0Zd042ayEK_f8dk5F31ri9o7fYSUIws2trF8"}

This result contains the JWT token that must then be passed to all future requests.

