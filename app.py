from flask import Flask
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from flask_jwt_extended import JWTManager
import logging
from logging.handlers import RotatingFileHandler
import os

# Configuration for environnment
config_file = "configuration.cfg"
if os.getenv("DEV") and os.getenv("DEV") in ("f", "false", "False"):
    config_file = "configuration_prod.cfg"
print("Configuration file is {}".format(config_file))

# Global variables
JSON_SCHEMA_DIR = "assets/json_schemas"

# Creating flask app and api
app = Flask(__name__)
app.config.from_pyfile(config_file)
api = Api(app)

# SQLAlchemy configuration
db = SQLAlchemy(app)

# JWT configuration
jwt = JWTManager(app)
app.config["JWT_BLACKLIST_TOKEN_CHECKS"] = ["access", "refresh"]

# Configuration of logging
logger = app.logger
logger.setLevel(logging.INFO)
handler = RotatingFileHandler("bro.log", maxBytes=1000000, backupCount=2)
logger.addHandler(handler)

# These imports come after configuration in order to make global variables availables
from models.models_autoload import loaded_models
from models.RevokedTokenModel import RevokedTokenModel
from resources.resources import register_api_points


# Needed functions
def load_models():
    logger.info("Loading models ...")
    loaded = loaded_models()
    logger.info("{} models loaded : {}".format(len(loaded), str(loaded)))


@app.before_first_request
def create_tables():
    logger.info("Creating models in the database ...")
    db.create_all()


@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token["jti"]
    return RevokedTokenModel.is_jti_blacklisted(jti)


@jwt.expired_token_loader
def handle_expired_error():
    return {"message": "Token has expired"}, 401


load_models()
register_api_points()
