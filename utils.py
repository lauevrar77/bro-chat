"""
    Utils function for the project
"""

import string
import random


def generate_pasword(
    size: int, chars=string.ascii_letters + string.digits + string.punctuation
) -> str:
    """Generates a random password of the given size

    Parameters
    ----------
    size: the size of the password to generate

    Returns
    -------
    the generated password

    """
    chars = chars.replace("\\", "").replace('"', "").replace("'", "")
    return "".join(random.choice(chars) for _ in range(size))
