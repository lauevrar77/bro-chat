from app import db


class BaseModel(db.Model):
    """Base class for metahub model, add some methods to it"""

    __abstract__ = True

    def save_to_db(self) -> None:
        """Updates this in the db"""
        db.session.add(self)
        db.session.commit()

    @classmethod
    def find_by(cls, **criterion):
        """
        Finds an object on a specific value
            :param criterion: the criteria as a dict {col: value}
        """

        return cls.query.filter_by(criterion)

    @classmethod
    def select_all(cls):
        """
        Returns all objects of this type
        """

        return cls.query.all()

    @classmethod
    def delete_all(cls):
        """
        Deletes all models of this kind
        """

        try:
            db.session.query(cls).delete()
            db.session.commit()
            return True
        except Exception:
            return False

    @classmethod
    def delete(cls, to_remove):
        """
        Removes the given object. This object must be an instance of the class
            :param to_remove: The object to remove.
        """

        assert isinstance(to_remove, cls)

        db.session.delete(to_remove)
        db.session.commit()
