"""The aim of this module is to autoload models so that sqlachemy can discover it and 
   make it's mapping"""


# These imports seems to be unused but they are necessary for sqlachemy autoload !
from app import db
from models.UserModel import UserModel
from models.GroupModel import GroupModel
from models.BroModel import BroModel

def loaded_models() -> None:
    """
    Returns the list of loaded models
    """

    return list(filter(lambda x: not x.startswith("_"), db.Model._decl_class_registry))
