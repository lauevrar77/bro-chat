from app import db
from models.models import BaseModel


class RevokedTokenModel(BaseModel):
    """Model for revoked JWT tokens"""

    __tablename__ = "revoked_tokens"
    id = db.Column(db.Integer, primary_key=True)
    jti = db.Column(db.String(120))

    def add(self):
        db.session.add(self)
        db.session.commit()

    @classmethod
    def is_jti_blacklisted(cls, jti: str) -> None:
        """
        Returns True if the token is black listed and false otherwise
            :param jti: The given token
        """

        query = cls.query.filter_by(jti=jti).first()
        return bool(query)
