from app import db
from typing import Tuple, Union
from sqlalchemy.orm import relationship
from models.models import BaseModel


class BroModel(BaseModel):
    """Model for a group"""

    __tablename__ = "bro_messages"

    id = db.Column(db.Integer, primary_key=True)
    message = db.Column(db.Text(), nullable=False)
    timestamp = db.Column(db.Integer, nullable=False)
    from_user_id = db.Column(db.Integer, db.ForeignKey("users.id"))
    from_user = db.relationship(
        "UserModel",
        foreign_keys=[from_user_id]
    )
    to_user_id = db.Column(db.Integer, db.ForeignKey("users.id"))
    to_user = db.relationship(
        "UserModel",
        foreign_keys=[to_user_id]
    )

    @classmethod
    def find_by_name(cls, name: str) -> Union["AttributeModel", None]:
        """Retrieve an AttributeModel via the name of the attribute

        Args:
            name: the name of the attribute
        Returns:
            The appropriate AttributeModel if it exists, None otherwise"""
        return cls.query.filter_by(name=name).first()
