from app import db
from models.models import BaseModel
from sqlalchemy.orm import relationship, backref
from passlib.hash import pbkdf2_sha256 as sha256


class UserModel(BaseModel):
    """Model for a user"""

    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(120), nullable=False)
    group_id = db.Column(db.Integer, db.ForeignKey("groups.id"))
    group = db.relationship("GroupModel", back_populates="users")
    sended_messages = relationship("BroModel", foreign_keys="BroModel.from_user_id")
    received_messages = relationship("BroModel", foreign_keys="BroModel.to_user_id")

    def change_password(self, new_password):
        """
        Changes the user password
            :param new_password: 
        """
        self.password = UserModel.generate_hash(new_password)
        db.session.commit()

    def is_admin(self):
        """
        Returns True if user is an admin false otherwise
        """

        return self.group is not None and self.group.name == "admin"

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    @classmethod
    def find_by_username(cls, username):
        return cls.query.filter_by(username=username).first()

    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                "username": x.username,
                "messages": len(x.sended_messages) + len(x.received_messages),
            }

        return {"users": list(map(lambda x: to_json(x), UserModel.query.all()))}

    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()
            db.session.commit()
            return {"message": "{} row(s) deleted".format(num_rows_deleted)}
        except Exception:
            return {"message": "Something went wrong"}

    @staticmethod
    def generate_hash(password):
        """
        Generates the hash of the given password
            :param password: the given password
        """

        return sha256.hash(password)

    @staticmethod
    def verify_hash(password, hash):
        """
        Verify the hash of a password
            :param password: the plain text password
            :param hash: the hash version of the password
        """

        return sha256.verify(password, hash)
