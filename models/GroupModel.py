from app import db
from typing import Tuple, Union
from sqlalchemy.orm import relationship
from models.models import BaseModel


class GroupModel(BaseModel):
    """Model for a group"""

    __tablename__ = "groups"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), nullable=False)
    users = relationship("UserModel")

    @classmethod
    def find_by_name(cls, name: str) -> Union["AttributeModel", None]:
        """Retrieve an AttributeModel via the name of the attribute

        Args:
            name: the name of the attribute
        Returns:
            The appropriate AttributeModel if it exists, None otherwise"""
        return cls.query.filter_by(name=name).first()
